#include <iostream>
#include <list>
#include <algorithm>
#include <queue>
#include <map>
using namespace std;

struct node {
	int data;
	int horizontalDistance;
	struct node* left;
	struct node* right;
	
};

struct node* newNode(int data) {
	struct node* anode = new struct node;
	anode->data = data;
	anode->left = NULL;
	anode->right = NULL;

	return anode;
}

void insert(struct node** rootRef, int data) {
	if (*rootRef == NULL) { //there is no right/left children
		*rootRef = newNode(data);
	}
	else {
		if (data < (*rootRef)->data) {  //if data is less than root
			insert(&(*rootRef)->left, data);
		}
		else {
			insert(&(*rootRef)->right, data); //if data is greater than root
		}
	}
}

void printInOrder(struct node* root) {

	if (root == NULL) return;
	printInOrder(root->left);
	cout << root->data << " ";
	printInOrder(root->right);
}

void printPostOrder(struct node* root) {

	if (root == NULL) return;
	printInOrder(root->left);
	printInOrder(root->right);
	cout << root->data << " ";
}

void printPreOrder(struct node* root) {

	if (root == NULL) return;
	cout << root->data << " ";
	printInOrder(root->left);
	printInOrder(root->right);
}

int size(struct node* root) {
	if (root == NULL) return 0;
	return (size(root->left) + size(root->right) + 1);

}

int max(int a, int b) {
	if (a > b) return a;
	if (b > a) return b;
	return a;
}

int height(struct node* root) {
	
	if (root == NULL) return 0;
	return (1 + max(height(root->left), height(root->right)));

}

//Find the smallest value in BST (With a loop)
int findMinValueLoop(struct node* root) {
	if (root == NULL) {
		return 0;
	}
	else {
		struct node* current = root;
		while (current->left != NULL) {
			current = current->left;
		}
		return current->data;
	}
}

//Find the smallest value in BST (Recursively)
int findMinValueRec(struct node* root) {

	if (root == NULL) {
		return 0;
	}
	else {
		if (root->left == NULL) {
			return root->data;
		}
		return findMinValueRec(root->left);
	}

}

//Find the maximum value in BST (Recursively)
int findMaxValueRec(struct node* root) {

	if (root == NULL) {
		return 0;
	}
	else {
		if (root->right == NULL) {
			return root->data;
		}
		return findMaxValueRec(root->right);
	}

}

struct node** getSuccessor(struct node** root) {
	struct node** temp = root;
	while ((*temp)->right != NULL) {
		temp = &(*temp)->right;
	}
	return temp;

}

void deleteNode(struct node** root, int target) {

	if (*root != NULL) {
		if (target == (*root)->data) {
			if ((*root)->right == NULL) {
				*root = (*root)->left;
			}
			else if ((*root)->left == NULL) {
				*root = (*root)->right;
			}
			else {
				struct node** successor = getSuccessor(&(*root)->left);
				(*root)->data = (*successor)->data;
				deleteNode(successor, (*successor)->data);
			}
		}
		else if (target < (*root)->data) {
			deleteNode(&(*root)->left, target);
		}
		else { //if (target > (*root)->data)
			deleteNode(&(*root)->right, target);
		}

	}

}

bool checkBST(struct node* root, struct node* l = NULL, struct node* r = NULL) {

	if (root == NULL) return true;

	if (l != NULL && root->data <= l->data) return false; //	The left subtree of a node contains only nodes with keys lesser than the node�s key.

	if (r != NULL && root->data >= r->data) return false; //	The right subtree of a node contains only nodes with keys greater than the node�s key.

	return checkBST(root->left, l, root) && checkBST(root->right, root, r); //recurse through the node

// If a node is a left child, then its key and the keys of the nodes in its right subtree are less than its parent�s key.
// If a node is a right child, then its keyand the keys of the nodes in its left subtree are greater than its parent�s key.
	
//recursion to check if it is a bst or not
//print if it is a bst or not
}

int* findNodeAddress(struct node* root, int userValue) {
	
	if (root == NULL) return NULL;

	findNodeAddress(root->left, userValue);

	if (userValue == root->data) {
		//cout << root->data;
		return &root->data;
	}

	findNodeAddress(root->right, userValue);

	if (userValue == root->data) {
		//cout << root->data;
		return &root->data;
	}
	//get value from user
	//check to see if there is the actual value
	//if so, print address of the node where the value is located
	//if not, return NULL
}

int findUserSmallest(struct node* root, int k) {

	if (root == NULL) return 0;

	int count = size(root->left) + 1;
	if (count == k) return root->data;
	if (count > k) return findUserSmallest(root->left, k);

	return findUserSmallest(root->right, k - count);

	//recursion through the whole bst in order to check for the smallest node value
	//print it out

	//find kth smallest element
}

void getNodesInOrder(struct node* curr, struct node*& prev, int& ans) {
	
	if (curr == NULL) return; //checks to see if there is data in the node

	getNodesInOrder(curr->left, prev, ans); //recurses down the left side of the tree with (curr->left) input

	if (prev != NULL) //if prev doesn't = null, then answer equals current node data - prev node data, which will be stored in answer
		ans = std::min(curr->data - prev->data, ans); //min function allows us to subract prev from curr and store in ans
	prev = curr; //sets old prev value to curr

	getNodesInOrder(curr->right, prev, ans); //recurses down the left side of the tree with (curr-> right) input
}

int findMinAbsDiff(struct node* root) { //driver function after getting nodes in order

	node* prev = NULL; //set variable prev to no value

	int ans = INT_MAX; //stores final answer

	getNodesInOrder(root, prev, ans);

	return ans; //returns final answer


	//recursion to find the largest value, and the smallest value
	//take the absolute value of each, and subtract the smallest from the largest
	//find the minimum absolute difference in the tree
	//print it out
}

void printTopView(struct node* root) {

	if (root == NULL) return;

	queue<struct node*> q; //storing queue<struct node*> into "q"
	map<int, int> m; //storing map into "m"
	int horizontalDistance = 0; //incrementer
	root->horizontalDistance = horizontalDistance;

	q.push(root); //puts topmost node on the queue stack

	cout << "The top view of the tree is: \n";

	while (q.size()) {
		horizontalDistance = root->horizontalDistance;

		if (m.count(horizontalDistance) == 0)
			m[horizontalDistance] = root->data;
		if (root->left) {
			root->left->horizontalDistance = horizontalDistance - 1;
			q.push(root->left);
		}
		if (root->right) {
			root->right->horizontalDistance = horizontalDistance + 1;
			q.push(root->right);
		}
		q.pop();
		
		if (!q.empty()) root = q.front();


	}

	for (auto i = m.begin(); i != m.end(); i++) {
		cout << i->second << " ";
}
	//    1
	//   / \
	//  2    3
	// / \   / \
	// 4  5  6   7
	//Top view of the above binary tree is
	//4 2 1 3 7
	
	//example of the problem
}


int main() {

	struct node* root = NULL;
	insert(&root, 30);
	insert(&root, 50);
	insert(&root, 70);
	insert(&root, 10);
	insert(&root, 90);
	insert(&root, 40);
	insert(&root, 80);
	
	printTopView(root);

	cout << "In-order: ";
	printInOrder(root);
	cout << endl;
	cout << "Pre-order: ";
	printPreOrder(root);
	cout << endl;
	cout << "Post-order: ";
	printPostOrder(root);
	cout << endl;

	cout << "The size of the BST: " << size(root) << endl;
	cout << "The height of the BST: " << height(root) << endl;
	cout << "The smallest value of BST: " << findMinValueRec(root) << endl;
	cout << findUserSmallest(root, 3) << endl;
	cout << "this is the min abs diff" << findMinAbsDiff(root) << endl;
	deleteNode(&root, 90);
	cout << "Delete 90 " << endl;

	cout << "The address of the node is: " << findNodeAddress(root, 80) << endl;

	cout << "0 is not a BST, 1 is a BST: " << checkBST(root, NULL, NULL) << endl;

	cout << "Maximum: " << findMaxValueRec(root) << endl;
	cout << "Minimum: " << findMinValueRec(root) << endl;
	return 0;

}